*** Settings ***
Library     SeleniumLibrary
Library     Collections

*** Variables ***
${url}  https://www.google.fr/
${id}  LrzXr kno-fv wHYlTd z8gr9e

*** Keywords ***
Obtain data info
    [Arguments]  ${element_to_search}
    [Return]  ${text_data}
    ${path_element}  Set Variable If
    ...  "${element_to_search}"=="Budget"  kc:/film/film:budget
    ...  "${element_to_search}"=="Realisateur"  kc:/film/film:director
    ...  "${element_to_search}"=="Date"  kc:/film/film:theatrical region aware release date
    ...  "${element_to_search}"=="Compositeur"  ss:/webfacts:musiqu
    ...  "${element_to_search}"=="Bande originale"  kc:/film/film:music
    ${text_data}  Get Text  //div[@data-attrid="${path_element}"]/div/div/div/span[@class="${id}"]

Log data
    [Arguments]  ${movie_search}
    [Return]  ${data_info_dict}
    #${movie_id}  Set Variable If
    #...  "${movie_search}"=="Jurassic World 3"  ${jw3_id}
    ${budget}  Run Keyword If  "${movie_search}"=="Jurassic World 3"  Obtain data info  Budget
    ...  ELSE  Set Variable  N.A.
    #Log To Console  Le film ${movie_search} a couté : ${budget}
    ${movie_date}  Obtain data info  Date
    #Log To Console  Le film ${movie_search} sort le : ${movie_date}
    ${realisator}  Obtain data info  Realisateur
    #Log To Console  Le réalisateur de ${movie_search} est : ${realisator}
    ${music_compositor}  Run Keyword If  "${movie_search}"=="Jurassic Park 1"  Obtain data info  Bande originale
    ...  ELSE IF  "${movie_search}"!="Jurassic World 2"  Obtain data info  Compositeur
    ...  ELSE  Set Variable  N.A.
    #Log To Console  Le compositeur de ${movie_search} est : ${music_compositor}
    ${data_info_dict}   Create Dictionary  budget=${budget}  date=${movie_date}  real=${realisator}  music=${music_compositor}


Navigate to website in Google
    [Arguments]  ${movie_search}
    Open Browser    ${url}    browser=chrome
    Sleep  1
    Click Button    //button[@id="L2AGLb"]
    Input Text  //input[@class="gLFyf gsfi"]  ${movie_search}
    Press keys  //input[@class="gLFyf gsfi"]  RETURN
    ${data_info_dict}  Log data  ${movie_search}
    Log To Console  ${movie_search} réalisé par ${data_info_dict.real}, est sorti le ${data_info_dict.date}, composé par ${data_info_dict.music}, pour un budget de ${data_info_dict.budget}
    Close Browser
    #Reload Page

*** Test Cases ***
Open website
    Navigate to website in Google  Jurassic Park 1
    Navigate to website in Google  Jurassic Park 2
    Navigate to website in Google  Jurassic Park 3
    Navigate to website in Google  Jurassic World 1
    Navigate to website in Google  Jurassic World 2
    Navigate to website in Google  Jurassic World 3



