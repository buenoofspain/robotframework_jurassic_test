*** Settings ***
Library     SeleniumLibrary
Library     Collections

*** Variables ***
#${ma_variable}  toto
#${age}  20
@{ma_list}  alpha   beta    delta

*** Test Cases ***
#petit test
    #Log To Console  ${ma_variable} est un petit scout
    #${ma_nouvelle_variable}     Set Variable    youpi

    #Créer une variable de comptage
    #${maj_age}  Evaluate  ${age}+1
    #Log To Console  ${ma_nouvelle_variable} est un petit scout de ${maj_age} ans
    #Log To Console  ${ma_list}
    #Log To Console  Ma liste ${ma_list} est composée de ${ma_list}[0] ${ma_list}[1] et ${ma_list}[2]

    #Créer une liste
    #${nouvelle_liste}   Create List  test1  test2   test3
    #Log To Console  ${nouvelle_Liste}

    # Créer un dictionnaire
    #${mon_dict}   Create Dictionary  test1=abc  test2=def   test3=ghi
    #Log To Console  ${mon_dict}

    # Créer un dictionnaire avec une liste
    #${mon_new_dict}   Create Dictionary  test1=abc  test2=${ma_list}
    #Log To Console  ${mon_newdict.test2}[1]

    # Obtenir la longueur d'une liste
    #${len_list}  Get Length  ${ma_list}
    #Log To Console  ${len_list}

    # Ajouter un élément dans une liste
    #Append To List  ${ma_list}  omega
    #Log To Console  ${ma_list}

    # Récupérer les valeurs d'un dictionnaire à partir de la clé
    #${mon_dict}   Create Dictionary  test1=abc  test2=def   test3=ghi
    #Log To Console  ${mon_dict}
    #Keep In Dictionary  ${mon_dict}  test1  test2
    #Log To Console  ${mon_dict}

    # ajouter de nouvelles clés:valeurs dans un dictionnaire
    #Set To Dictionary  ${mon_dict}  test4=jkl
    #Log To Console  Mon nouveau dictionnaire avec test4 : ${mon_dict}

    # Remplacer une valeur dans une liste à une position précise
    #${ma_list}  Create List  toto  titi  riri  fifi
    #Log To Console  ${ma_list}
    #Set List Value  ${ma_list}  0  tata
    #Log To Console  ${ma_list}

Navigate to hoyolab
    Open Browser    https://www.hoyolab.com/    browser=chrome


    Click button





